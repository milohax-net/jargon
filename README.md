This is a mirror of the Jargon File.

You can see the original at http://www.jargon.org  or http://catb.org/jargon.

This copy was cloned in 2016.

In 2018 the glossary was [transcoded from ISO-8859-1 to utf-8](https://milosophical.me/blog/2018/latin1-to-utf8.html).

## Patches

There are some `.patch` files which were applied after I mirrored the jargon file. These are:

 * `milo-mirror.patch` This adds a heading to make it clear to readers that this is a mirror of Eric Raymond's original site, and to offer a link back to my own home page
 * `milo-jargoogle.patch` Patches `jargoogle.html` to be a Google site-search for `milosophical.me` instead of `catb.org`
 * `milo-no-old-versions.patch` This patch merely changes the `href` for the old Jargon File versionsback to the absolute URL for `catb.org`, since I'm not interested in mirroring those. I did add a link to their location in Bitbucket, but that is stale now (since about 2019).
